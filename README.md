# backstabbr-openings-scraper

Just a quick script to grab the opening moves from a Backstabbr game or a file of Backstabbr URLs and output a CSV

## Getting started

- Install Python3 if you don't have it already.

## Usage
- `python3 backstabbr-openings-scraper.py -h`
- `python3 backstabbr-openings-scraper.py -u hxxp://game_url`
- `python3 backstabbr-openings-scraper.py -U file_with_game_urls_one_per_line`
