import argparse
import ast
import os
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse

# argparse setup
parser = argparse.ArgumentParser(description="Scrape openings from Diplomacy games")
input_type = parser.add_mutually_exclusive_group(required=True)
input_type.add_argument(
    "-u", "--url", type=str, default=argparse.SUPPRESS, help="Backstabbr URL"
)
input_type.add_argument(
    "-U", "--urls", type=str, default=argparse.SUPPRESS, help="File with list of Backstabbr URLs"
)
args = parser.parse_args()


# Prepare the list of game URLs
urls = ""

if hasattr(args, "urls"):
    with open(args.urls) as f:
        urls = f.read().split()
else:
    urls = [args.url]


# Scrape the openings
backstabbr_url = "https://www.backstabbr.com"
print("gameURL,country,orders")


for url in urls:

    # Prep URL for request
    url = urlparse(url)
    opening_path = "/".join(url.path.split("/")[:4]) + "/1901/spring"

    S01_url = backstabbr_url + opening_path
    page = requests.get(S01_url)

    # Parse response to get accurate orders from a hidden Script tag
    soup = BeautifulSoup(page.content, "html.parser")
    orders_script_tag = soup.find_all("script")

    for tag in orders_script_tag:
        if 'orders = ' in tag.text:
            orders_script_tag = tag
            break

    orders_text = orders_script_tag.text.split("orders = ")[1].split(";")[0]
    orders_dict = dict(sorted(ast.literal_eval(orders_text).items()))

    for country, orders in orders_dict.items():
        sorted_orders = dict(sorted(orders.items()))

        print(S01_url + "," + country + ",", end='')
        orders_csv = []
        for order in sorted_orders:
            order_type = sorted_orders[order]["type"]

            if sorted_orders[order]["type"] == "HOLD":
                s = order + "-HOLD"
            elif sorted_orders[order]["type"] == "SUPPORT":
                s = order + " S " + sorted_orders[order]["from"] + "-" + sorted_orders[order]["to"]
            else:
                s = order + "-" + sorted_orders[order]["to"]

            orders_csv.append(s)
        print("|".join(orders_csv))
